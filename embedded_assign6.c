#include<reg51.h>
unsigned char led[] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09};
int count = 0;
sbit led1 = P2^6;
sbit led2 = P2^7;
void delay(int tick){
	int i,j;
	for(i=0;i<tick;i++)
		for(j=0;j<400;j++);
}
void ext() interrupt 0 { 
	delay(1000);
	led1 = ~led1; 
	if(count != 0){
		count--;
		P0 = led[count];
	}
}

void ext2() interrupt 2 {
	delay(1000);
led2 = ~led2;
	if(count==10) count = 0;
	count++;
	P0 = led[count];
}



void main(){
	P0 = 0x00;
	IE=0x85;
	IT0 = 1;
	IT1 = 1;
	while(1){
		P1 = 0x01;
		delay(10000);

		P1 = 0;
		P1 = 0x02;
		delay(10000);

		P1=0;
		P1 = 0x04;
		delay(10000);

		P1=0;
		P1 = 0x08;
		delay(10000);
	}
}